SRCREV_metaenea ?= "28da254d1c5ae012a6e064671aa14850c2a21a25"
KENEABRANCH = "intel-4.9"
SRC_URI_append = " git://git@git.enea.com/linux/enea-kernel-cache.git;protocol=ssh;type=kmeta;name=metaenea;branch=${KENEABRANCH};destsuffix=enea-kernel-meta"

KERNEL_FEATURES_append = " features/udev/udev.scc"

# NFS boot support
KERNEL_FEATURES_append = " features/blkdev/net_blk_dev.scc"

# Intel 10G ports(SoC)
KERNEL_FEATURES_append_corei7-64-intel-common = " features/ixgbe/ixgbe_y.scc"
KERNEL_FEATURES_append_corei7-64-intel-common = " features/dca/dca_y.scc"

# NMVe SSD
KERNEL_FEATURES_append = " features/nvme/nvme.scc"

#IPv4 waiting for carrier on
KERNEL_FEATURES_append = " patches/ipv4/ipv4wait.scc"
