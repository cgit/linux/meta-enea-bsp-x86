require common/recipes-kernel/linux/linux-intel_4.9.bb
require linux-intel.inc

# Debug tools support
KERNEL_FEATURES_append = " features/debug/debug_kernel_y.scc"
KERNEL_FEATURES_append = " features/kgdb/kgdb_y.scc"
KERNEL_FEATURES_append = " features/lttng/lttng_y.scc"
KERNEL_FEATURES_append = " features/latencytop/latencytop_y.scc"
KERNEL_FEATURES_append = " features/perf/perf_y.scc"
KERNEL_FEATURES_append = " features/systemtap/systemtap_y.scc"
KERNEL_FEATURES_append = " features/oprofile/oprofile_y.scc"
