SRCREV_metaenea ?= "5057d9d3837fc35814a402b9c0384a064d260d05"
KENEABRANCH = "yocto-4.9"
SRC_URI_append = " git://git@git.enea.com/linux/enea-kernel-cache.git;protocol=ssh;type=kmeta;name=metaenea;branch=${KENEABRANCH};destsuffix=enea-kernel-meta"

KERNEL_FEATURES_append = " features/udev/udev.scc"

# NFS boot support
KERNEL_FEATURES_append = " features/blkdev/net_blk_dev.scc"

# Virtual function support
KERNEL_FEATURES_append = " features/ixgbevf/ixgbevf_y.scc"

#IPv4 waiting for carrier on
KERNEL_FEATURES_append = " patches/ipv4/ipv4wait.scc"
