COMPATIBLE_MACHINE_inteld1521 = "inteld1521"
COMPATIBLE_MACHINE_qemux86-64 = "qemux86-64"

# lspci - required by dpdk tools/scripts
RDEPENDS_${PN} += "pciutils"

export RTE_TARGET="${@bb.utils.contains("TUNE_FEATURES", "m64", "x86_64-native-linuxapp-gcc", "i686-native-linuxapp-gcc", d)}"
export ETHTOOL_LIB_PATH = "${S}/examples/ethtool/lib/${RTE_TARGET}/lib/"

# Overrides the default value set to DPDK_TARGET_MACH in dpdk.inc,
# with the purpose adding 'inteld1521' to 'multiarch_options'
export DPDK_TARGET_MACH = "${@get_dpdk_target(bb,d)}"

def get_dpdk_target(bb, d):
    target_arch = d.getVar('MACHINE_ARCH', True)
    multiarch_options = {
        "mohonpeak64":"atm",
        "mohonpeak32":"atm",
        "crystalforest":  "ivb",
        "intel_corei7_64": "hsw",
        "inteld1521": "hsw",
        "qemux86_64": "default",
    }

    if target_arch in multiarch_options :
        return multiarch_options[target_arch]
    return ""
